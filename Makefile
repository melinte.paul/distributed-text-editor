build:main

main:main.cpp
	mpic++ main.cpp -o main -std=c++17

test1:main
	mpirun -np 5 --oversubscribe ./main ./tests/in/input1.txt

test2:main
	mpirun -np 5 --oversubscribe ./main ./tests/in/input2.txt

test3:main
	mpirun -np 5 --oversubscribe ./main ./tests/in/input3.txt

test4:main
	mpirun -np 5 --oversubscribe ./main ./tests/in/input4.txt

test5:main
	mpirun -np 5 --oversubscribe ./main ./tests/in/input5.txt

test:main
	python3 checker.py