#include <iostream>
#include <fstream>
#include <mpi/mpi.h>
#include <map>
#include <vector>
#include <unistd.h>
#include <sstream>
#include <algorithm>

using namespace std;

static int numtasks, MPI_rank;

//variabilele globale ce tin de MASTER
static string in_file_name, out_file_name;
static string nume_paragrafe[4] = {"horror", "comedy", "fantasy", "science-fiction"};
//Fiecare paragraf este salvat in acest map, si este scris in fisier la final
static map<int, string> rezultat_procesare;
pthread_mutex_t mutex_output;

//Functie ce verifica daca linia curenta este titlul unui paragraf si care este
//Daca nu este un paragraf returneaza -1, daca este returneaza indexul din nume_paragrafe
int is_nume_paragraf(string &linie) {
    if (linie.rfind(nume_paragrafe[0], 0) == 0) {
        return 0;
    }
    if (linie.rfind(nume_paragrafe[1], 0) == 0) {
        return 1;
    }
    if (linie.rfind(nume_paragrafe[2], 0) == 0) {
        return 2;
    }
    if (linie.rfind(nume_paragrafe[3], 0) == 0) {
        return 3;
    }
    return -1;
}

//Functia celor 4 threaduri din MASTER
void *master_func(void *arg) {
    long id = (long) arg;

    ifstream in_file(in_file_name);

    string line;
    string total_paragraf;

    int line_type;
    bool in_paragraf = false;
    bool own_paragraf = false;
    int paragraf_count = -1;

    while (std::getline(in_file, line)) {
        line_type = is_nume_paragraf(line);
        if (line_type >= 0 && !in_paragraf) {
            paragraf_count++;
            in_paragraf = true;
            if (line_type == id) {
                own_paragraf = true;
            }
        } else if (line.c_str()[0] == 0) {
            if (own_paragraf) {
                //Trimiterea si primirea intregului paragraf la worker
                MPI_Send(total_paragraf.c_str(),total_paragraf.size()+1,MPI_CHAR,id,id,MPI_COMM_WORLD);

                MPI_Status status;
                MPI_Probe(id,id,MPI_COMM_WORLD,&status);
                char* line_buffer = static_cast<char *>(malloc(sizeof(char) * (status._ucount + 1)));
                MPI_Recv(line_buffer,status._ucount+1,MPI_CHAR,id,id,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

                //Adaugarea la rezultat
                string final_paragraf = line_buffer;

                pthread_mutex_lock(&mutex_output);

                rezultat_procesare[paragraf_count] = nume_paragrafe[id];
                rezultat_procesare[paragraf_count].append("\n");
                rezultat_procesare[paragraf_count].append(final_paragraf);

                pthread_mutex_unlock(&mutex_output);

                total_paragraf = "";
            }

            in_paragraf = false;
            own_paragraf = false;
        } else {
            if (own_paragraf) {
                total_paragraf.append(line);
                total_paragraf.append("\n");
            }
        }
    }

    //Cand fisierul este terminat, MASTER trimite catre WORKER un mesaj de lungime 0
    MPI_Send(total_paragraf.c_str(),0,MPI_CHAR,id,id,MPI_COMM_WORLD);

    pthread_exit(nullptr);
}

//Variabilele ce tin de WORKERS
static long NUM_THREADS;

//Liniile primite
static vector<string>init_lines;
//Liniile ce vor fi trimise
static string *final_lines;

//Flag ce va fi setat pe 1 cand threadurile de procesare trebuie sa se inchida
static int flag_done = 0;
//Bariera dupa care se trece cand main_worker a terminat de primit si separat liniile
static pthread_barrier_t barrier_read;
//Bariera dupa care se trece cand secondary_workers au terminat de procesat
static pthread_barrier_t barrier_write;


void* main_worker(void* arg){
    long id = (long) arg;

    MPI_Status status;

    while(true){
        //Probe pentru a putea aloca spatiu
        MPI_Probe(4,MPI_rank,MPI_COMM_WORLD,&status);
        //Daca lungimea este 0, am terminat de procesat fisierul
        if(status._ucount == 0){
            flag_done = 1;
            pthread_barrier_wait(&barrier_read);
            break;
        }
        char* line_buffer = static_cast<char *>(malloc(sizeof(char) * (status._ucount + 1)));
        MPI_Recv(line_buffer,status._ucount+1,MPI_CHAR,4,MPI_rank,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

        //Separarea liniilor
        init_lines = {};
        string paragraf = line_buffer;
        stringstream stream_paragraf(paragraf);
        string line;

        while(getline(stream_paragraf,line)){
            init_lines.push_back(line);
        }

        final_lines = new string[init_lines.size()];

        pthread_barrier_wait(&barrier_read);
        pthread_barrier_wait(&barrier_write);

        //Legarea la loc a paragrafului si trimiterea la MASTER
        string final_paragraf;
        for(int i = 0;i<init_lines.size();i++){
            final_paragraf.append(final_lines[i]);
            final_paragraf.append("\n");
        }

        MPI_Send(final_paragraf.c_str(),final_paragraf.size()+1,MPI_CHAR,4,MPI_rank,MPI_COMM_WORLD);
    }

    pthread_exit(nullptr);
}

//Verificam daca c este vocala
bool isVowel(char c){
    if(c == 'a' ||
    c == 'e' ||
    c == 'i' ||
    c == 'o' ||
    c == 'u' ||
    c == 'A' ||
    c == 'E' ||
    c == 'I' ||
    c == 'O' ||
    c == 'U')
        return true;
    return false;
}

//Verificam daca c este o litera
bool isLetter(char c){
    if((c >= 'a' && c<= 'z') || (c >= 'A' && c<='Z'))
        return true;
    return false;
}

//Cele 4 functii de transformare
string horror_transform(string line){
    string new_line;
    string word,new_word;
    stringstream stream_line(line);

    while(getline(stream_line,word,' ')){
        new_word = "";
        for(int i = 0; i<word.size();i++){
            new_word.push_back(word[i]);
            if(!isVowel(word[i]) && isLetter(word[i])){
                new_word.push_back(tolower(word[i]));
            }
        }

        new_line.append(new_word);
        new_line.append(" ");
    }

    return new_line;
}

string comedy_transform(string line){
    string new_line;
    string word;
    stringstream stream_line(line);

    while(getline(stream_line,word,' ')){
        for(int i = 1; i<word.size();i+=2){
            word[i] = toupper(word[i]);
        }

        new_line.append(word);
        new_line.append(" ");
    }

    return new_line;
}


string fantasy_transform(string line){
    string new_line;
    string word;
    stringstream stream_line(line);

    while(getline(stream_line,word,' ')){
        word[0] = toupper(word[0]);
        new_line.append(word);
        new_line.append(" ");
    }

    return new_line;
}

string sf_transform(string line){
    string new_line;
    string word;
    stringstream stream_line(line);
    int count = 0;

    while(getline(stream_line,word,' ')){
        count++;
        if(count%7 == 0){
            reverse(word.begin(),word.end());
            new_line.append(word);
            new_line.append(" ");
        }else{
            new_line.append(word);
            new_line.append(" ");
        }
    }

    return new_line;

}

void* secondary_worker(void* arg){
    long id = (long) arg;

    while(1){
        //Dupa ce s-a terminat de citit paragraful, fiecare thread incepe procesarea
        pthread_barrier_wait(&barrier_read);
        //Daca flagul este setat, putem inchide threadul
        if(flag_done == 1){
            break;
        }

        //Paragraful este impartit in blocuri de (NUM_THREADS-1)*20 de linii
        //Fiecare thread isi transforma cele 20 de linii si trece la urmatorul bloc
        for(int i = id * 20; i < init_lines.size();i+=20 * (NUM_THREADS-1)){
            for(int j = 0;j<20 && i+j<init_lines.size();j++){
                switch (MPI_rank) {
                    case 0 : final_lines[i+j] = horror_transform(init_lines[i+j]);
                             break;
                    case 1 : final_lines[i+j] = comedy_transform(init_lines[i+j]);
                        break;
                    case 2 : final_lines[i+j] = fantasy_transform(init_lines[i+j]);
                        break;
                    case 3 : final_lines[i+j] = sf_transform(init_lines[i+j]);
                        break;
                }
            }
        }

        //Bariera de terminare a procesarii
        pthread_barrier_wait(&barrier_write);
    }

    pthread_exit(nullptr);
}

int main(int argc, char **argv) {
    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

    if (provided < MPI_THREAD_MULTIPLE) {
        cout << "MPI nu a putut porni cu support total de threaduri" << endl;
        return 1;
    }
    if (argc < 2) {
        cout << "Folosire : ./main nume_fisier , rulat prin MPI" << endl;
        return 1;
    }

    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &MPI_rank);
    if (numtasks < 5) {
        cout << "Rulati cu cel putin 5 taskuri, folositi --oversubscribe" << endl;
        return 1;
    }

    in_file_name = argv[1];
    out_file_name = argv[1];
    out_file_name.pop_back();
    out_file_name.pop_back();
    out_file_name.pop_back();
    out_file_name.append("out");

    //MASTER este procesul cu rank 4
    if (MPI_rank == 4) {
        pthread_t threads[4];
        pthread_mutex_init(&mutex_output, nullptr);

        for (long i = 0; i < 4; i++) {
            pthread_create(&threads[i], nullptr, master_func, (void *) i);
        }

        void *status;
        for (unsigned long thread : threads) {
            pthread_join(thread, &status);
        }

        pthread_mutex_destroy(&mutex_output);

        //Scrierea in fisier
        ofstream out_file(out_file_name);

        for (auto&[key, value] : rezultat_procesare) {
            out_file << value << endl;
        }
    }

    //Celelalte procese sunt workeri
    if (MPI_rank < 4) {
        NUM_THREADS = sysconf(_SC_NPROCESSORS_CONF);
        pthread_t threads[NUM_THREADS];

        pthread_barrier_init(&barrier_read, nullptr, NUM_THREADS);
        pthread_barrier_init(&barrier_write, nullptr, NUM_THREADS);

        for (long i = 0; i < NUM_THREADS-1; i++) {
            pthread_create(&threads[i], nullptr, secondary_worker, (void *) i);
        }
        pthread_create(&threads[(NUM_THREADS-1)], nullptr, main_worker, (void *) (NUM_THREADS-1));

        void *status;
        for (int i = 0; i < NUM_THREADS; i++) {
            pthread_join(threads[i], &status);
        }

        pthread_barrier_destroy(&barrier_read);
        pthread_barrier_destroy(&barrier_write);

    }

    MPI_Finalize();

    return 0;
}
